angular-inject
======
Keep your code DRY. Remove controller boilerplate with an inject directive.

With a few basic injectable services:
```
<script>
  angular
    .module('services', [])
    .service('employee', function () {
      this.name = 'employee';
      this.pay = angular.noop;
    })
    .service('user', function () {
      this.name = 'user';
      this.serve = angular.noop;
    })
    .service('customer', function () {
      this.name = 'customer';
      this.help = angular.noop;
    });
</script>
```

Per the angularjs docs:
```
https://docs.angularjs.org/guide/controller
Use controllers to:
- Set up the initial state of the $scope object.
- Add behavior to the $scope object.

Do not use controllers to:
- Manipulate DOM — Controllers should contain only business logic. Putting any presentation logic into Controllers significantly affects its testability. Angular has databinding for most cases and directives to encapsulate manual DOM manipulation.
- Format input — Use angular form controls instead.
- Filter output — Use angular filters instead.
- Share code or state across controllers — Use angular services instead.
- Manage the life-cycle of other components (for example, to create service instances).
```

Here is what the above concept should look like...
```
<script>
  angular
    .module('ok', ['services'])
    .controller('ExampleCtrl', function(employee, user, customer) {
      this.employee = employee;
      this.user = user;
      this.customer = customer;
    });
</script>

<div app="ok">
  <div ng-controller="ExampleCtrl as injected">
    {{injected.employee}},
    {{injected.user}},
    {{injected.customer}}
  <div>
</div>
```

This simple `inject` directive achives the same thing:
```
<script>
  angular.module('better', ['services', 'inject'])
</script>

<div app="better">
  <div inject="['first', 'second', 'third'] as injected">
    {{injected.employee}},
    {{injected.user}},
    {{injected.customer}}
  <div>
</div>
```
